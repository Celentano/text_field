<?php

// Make sure these strings are available in the translation files, since they're partly derived from variables at runtime.
_('text_field_title');
_('text_field_description');
_('left');
_('center');
_('right');

$defaults = new StdClass();
$defaults->fontSize = 100;
$defaults->textAlignment = 'center';

$content = getConfigValue('text_field_content');
$options = getConfigValue('text_field_options');

if (empty($options)) {
    $options = $defaults;
} else {
    $options = json_decode($options);
}

if (empty($options->fontSize)) {
    $options->fontSize = $defaults->fontSize;
}

?>

<p><?php echo _('Enter the text you want to display, choose its font size and alignment.'); ?></p>

<form id="text_field_form">
    <fieldset>
        <label for="text_field_alignment"><?php echo _('Choose how you want the text aligned in its cell.'); ?></label>
        <select name="text_field_alignment" id="text_field_alignment">
            <?php
            foreach (['left', 'center', 'right'] as $option) {
                if ($option === $options->textAlignment) {
                    print("<option selected value=\"$option\">". _($option));
                } else {
                    print("<option value=\"$option\">". _($option));
                }
            }
            ?>
        </select>
        <label for="text_field_fontsize"><?php echo _('Choose the scale at which you want your text to display. Larger values results in larger text.') ?></label>
        <input id="text_field_fontsize" type="number" pattern="\d*" step="10" name="text_field_fontsize" value="<?php echo $options->fontSize; ?>" placeholder="100" />
    </fieldset>
    <textarea id="text_field_content" rows="8" placeholder="<?php echo _('Enter your text here!'); ?>"><?php echo $content; ?></textarea>
</form>

<div class="block__add" id="text_field__edit">
	<button class="text-field__edit--button" href="#">
		<span><?php echo _('save'); ?></span>
	</button>
</div>
