/**
 * Backend UI logic for the text_field/logo modal.
 */
// Uoload new logo images & handle preview.
$('#text_field__edit').click(function() {

    $.post('setConfigValueAjax.php', {'key' : 'text_field_content', 'value' : $('#text_field_content').val()});

    let sizeInput = $('#text_field_fontsize').val();
    let options = {
        fontSize:  sizeInput === '' ? 100 : sizeInput,
        textAlignment: $('#text_field_alignment').val(),
    }

    $.post('setConfigValueAjax.php', {'key': 'text_field_options', 'value': JSON.stringify(options)});


    $('#ok').show(30, function() {
        $(this).hide('slow');
    })
});
