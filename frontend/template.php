<?php

$content = getConfigValue('text_field_content');
$options = json_decode(getConfigValue('text_field_options'));
?>

<div 
    id="text_field" 
    style="font-size: <?php echo $options->fontSize; ?>%; 
        text-align: <?php echo $options->textAlignment; ?>; 
        line-height: <?php echo ceil($options->fontSize / 100); ?>.5rem">
    <?php echo $content; ?>
</div>