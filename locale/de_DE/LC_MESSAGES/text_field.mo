��          t      �         1     ^   C  G   �     �                               .  �  ?  >   7  u   v  `   �     M     g     n     {  	   �  5   �     �                               
                         	    Choose how you want the text aligned in its cell. Choose the scale at which you want your text to display. Larger values results in larger text. Enter the text you want to display, choose its font size and alignment. Enter your text here! center left right save text_field_description text_field_title Project-Id-Version: mirr.OS Modul Branding
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2017-10-11 18:42+0200
PO-Revision-Date: 2017-10-11 18:42+0200
Last-Translator: Tobias Grasse <mail@tobias-grasse.net>
Language-Team: Tobias Grasse <tg@glancr.de>
Language: de_DE
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Poedit-SourceCharset: UTF-8
X-Generator: Poedit 2.0.3
X-Poedit-Basepath: ../../..
X-Poedit-SearchPath-0: .
 Wähle wie du den Text innerhalb der Kachel ausrichten willst. Wähle die Skalierung, mit der du deinen Text anzeigen lassen willst. Größere Werte resultieren in größerem Text. Gibt den Text ein, den du angezeigt haben willst, und wähle die Schriftgröße und Ausrichtung. Gib hier deinen Text ein! mittig linksbündig rechtsbündig speichern Zeige auf deinem glancr beliebigen Text oder HTML an! Textfeld 